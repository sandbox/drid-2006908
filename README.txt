DESCRIPTION
-----------
This module allows to control visibility of nivo_slider slides based on
currently selected language.

A drop-down form element is added on each slide allowing the slide to
be visible in one of the enabled languages as well as in all languages. 

Currently a small patch needs to be applied for this module to work.
See http://drupal.org/node/2006920 for details

Tested with nivo_slider 7.x-1.9

REQUIREMENTS
------------

Nivo Slider module is required for this module to work

http://drupal.org/project/nivo_slider

INSTALLATION
------------
Copy nivo_slider_language folder to you modules directory.

Apply patch to nivo_slider.

Copy nivo_slider_slider.patch into nivo_slider module folder
Use the following command to pathc nivo slider

patch includes/nivo_slider_slider.inc < nivo_slider_slider.patch

Enable nivo_slider_language module
